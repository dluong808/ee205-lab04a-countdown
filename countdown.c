///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @Daniel Luong<dluong@hawaii.edu>
// @date   February 4 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Includes struc tm


// delay function
void delay(int number_of_seconds)
{
   //Turns your seconds inputted into comparable Clocks value
   int  sec2clk = CLOCKS_PER_SEC * number_of_seconds;

   //Storing Start time
   clock_t start_time = clock();

   //
   while (clock() < start_time + sec2clk)
      ;
}





int main(int argc, char* argv[]) {
   
   time_t current_time;
   struct tm *diff_struc;
   time_t ref_time_t;
   //Make reference time structure
   struct tm ref_time = {.tm_year=110,.tm_mon=1,.tm_mday=1,.tm_hour=1,
      .tm_min=1,.tm_sec=3};
   //Convert into a time_t
   ref_time_t = timegm(&ref_time);
   
   printf("\nReference time: HST %s\n",asctime(&ref_time));



   while("TRUE"){

   // Get Current Time in seconds
   current_time = time (NULL);

   //Get difference of seconds of current time and reference time
   
   time_t difference_time = difftime(current_time,ref_time_t);

   diff_struc= localtime(&difference_time);
   // - 70 years as Local time starts from 1970 and normal tm_year starts from 1900
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",
   diff_struc->tm_year-70   ,diff_struc->tm_mday,diff_struc->tm_hour,diff_struc->tm_min,diff_struc->tm_sec);
   
   delay(1);

   
   


   




  


   
   }
   return 0;
}
